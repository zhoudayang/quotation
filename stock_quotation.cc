#include "stock_quotation.h"

#include "loop_singleton.h"
#include <muduo/net/EventLoop.h>
#include <muduo/base/Logging.h>


using namespace fit_stock_sina;
using std::placeholders::_1;
using std::placeholders::_2;

///!!! never call stock_quotation constructor in io thread
stock_quotation::stock_quotation(const std::vector<std::string> &stock_codes)
  : client_nums_((stock_codes.size() + BUCKET_SIZE - 1) / BUCKET_SIZE),
    sina_clients_(),
    latch_(),
    quotation_datas_(),
    stock_codes_(stock_codes),
    complete_(true)
{
  assert(!stock_codes.empty());
  quotation_datas_.reserve(stock_codes_.size());
}

void stock_quotation::runInClient(std::unordered_map<std::string, struct sina_exposure> data, bool complete) {
  quotation_datas_.insert(data.begin(), data.end());
  complete_ = complete;
  assert(latch_);
  latch_->countDown();
}

void stock_quotation::wait()
{
  assert(latch_);
  latch_->wait();
}

void stock_quotation::destructInThread(std::vector<std::shared_ptr<sina_client>> sina_clients)
{
  //do nothing
  LOG_INFO << "destruct sina clients in io thread";
}

stock_quotation::~stock_quotation()
{
  auto clients = sina_clients_;
  sina_clients_.clear();
  loop_singleton::instance()->loop()->queueInLoop(std::bind(&stock_quotation::destructInThread, clients));
  LOG_INFO << "~stock_quotation";
}

void stock_quotation::runInClientWeak(const std::weak_ptr<stock_quotation>& wkPtr,
                                      std::unordered_map<std::string, struct sina_exposure> data, bool complete)
{
  auto ptr = wkPtr.lock();
  if(ptr)
  {
    ptr->runInClient(data, complete);
  }
}

std::pair<std::unordered_map<std::string, struct sina_exposure>, bool> stock_quotation::get_quotations() {
  quotation_datas_.clear();
  latch_.reset(new muduo::CountDownLatch(client_nums_));
  loop_singleton::instance()->loop()->queueInLoop(std::bind(&stock_quotation::queueInLoop, this));
  wait();
  return std::make_pair(quotation_datas_, complete_);
}

void stock_quotation::queueInLoop()
{
  auto begin = stock_codes_.begin();
  auto end = stock_codes_.end();
  for(int i = 0; i < client_nums_; ++ i)
  {
    if(i == (client_nums_ - 1))
    {
      sina_clients_.push_back(std::shared_ptr<sina_client>(new sina_client({begin, end}, std::bind(&stock_quotation::runInClientWeak, std::weak_ptr<stock_quotation>(shared_from_this()), _1, _2))));
    }
    else
    {
      sina_clients_.push_back(std::shared_ptr<sina_client>(new sina_client({begin, begin + BUCKET_SIZE}, std::bind(&stock_quotation::runInClientWeak, std::weak_ptr<stock_quotation>(shared_from_this()), _1, _2))));
      begin += BUCKET_SIZE;
    }
  }
}
