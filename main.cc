#include "loop_singleton.h"

#include <muduo/base/Logging.h>

#include "stock_list_helper.h"
#include "stock_quotation.h"
using namespace std;
using namespace fit_stock_sina;


void runEveryFiveSeconds(const std::shared_ptr<stock_quotation>& ptr)
{
  LOG_INFO << ptr->get_quotations().first.size();
}

int main()
{
  auto helper = std::make_shared<stock_list_helper>();
  auto stock_codes = helper->stock_codes();
  auto quotation = std::make_shared<stock_quotation>(stock_codes);

  while(true)
    runEveryFiveSeconds(quotation);

}
