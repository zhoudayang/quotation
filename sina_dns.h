#ifndef FIT_QUOTATION_SINA_DNS_H
#define FIT_QUOTATION_SINA_DNS_H

#include <boost/noncopyable.hpp>
#include <muduo/net/InetAddress.h>

// dns getter, blocking way, singleton
namespace fit_stock_sina
{
class sina_dns : boost::noncopyable
{
 public:
  static sina_dns* instance();

  muduo::net::InetAddress address() const { return addr_; }

 private:
  // constructor
  sina_dns();

  // destructor
  ~sina_dns() = default;

  static void init();

  static void destroy();

  static pthread_once_t ponce_;
  static sina_dns*      value_;

 private:
  muduo::net::InetAddress addr_;
};
}
#endif
