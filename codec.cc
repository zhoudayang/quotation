#include "codec.h"
#include "util.h"

#include <muduo/base/Logging.h>
using namespace fit_stock_sina;

using std::string;
namespace detail
{

const static string kInvalidResponseCodeStr = "kInvalidResponse Error";
const static string kNoErrorStr = "kNoError";
const static string kInvalidContentLengthStr = "kInvalidContentLength Error";
const static string kLengthConvertErrorStr = "kLengthConvert Error";
const static string kUnknownErrorStr = "kUnknown Error";

const std::string errorCodeToString(codec::ErrorCode errorCode)
{
  switch (errorCode)
  {
    case codec::kNoError: return kNoErrorStr;
    case codec::kLengthConvertError: return kLengthConvertErrorStr;
    case codec::kInvalidResponseCode: return kInvalidResponseCodeStr;
    case codec::kInvalidContentLength: return kInvalidContentLengthStr;
    default: return kUnknownErrorStr;
  }
}
}

codec::codec(const codec::MessageCallback &messageCb,
                                   const codec::ErrorCallback &errorCb)
    : messageCallback_(messageCb),
      errorCallback_(errorCb)
{

}

codec::codec(const codec::MessageCallback &messageCb)
    : messageCallback_(messageCb),
      errorCallback_(defaultErrorCallback)
{

}

// if error occurred, shutdown the connection
void codec::defaultErrorCallback(const muduo::net::TcpConnectionPtr& con,
                                            muduo::net::Buffer *,
                                            muduo::Timestamp,
                                            codec::ErrorCode code)
{
  LOG_ERROR << detail::errorCodeToString(code);
  con->shutdown();
}

void codec::onMessage(const muduo::net::TcpConnectionPtr &con,
                                 muduo::net::Buffer *buf,
                                 muduo::Timestamp receiveTime)
{
  while(Utils::find(buf, "HTTP/1.1") != nullptr && buf->findCRLF() != nullptr)
  {
    string http_response(buf->peek(), buf->findCRLF());
    if(http_response.find("200") == string::npos && http_response.find("OK") == string::npos)
    {
      errorCallback_(con, buf, receiveTime, kInvalidResponseCode);
      break;
    }
    const char* length_str = Utils::find(buf, "Content-Length:");
    if(length_str == nullptr)
    {
      LOG_TRACE << "cannot get content-length yet, wait";
      break;
    }
    const char* length_end = buf->findCRLF(length_str);
    if(length_end == nullptr)
    {
      LOG_TRACE << "content-length is not complete yet, wait";
      break;
    }
    string content_length(length_str, length_end);
    size_t colon_pos;
    if((colon_pos = content_length.find(':')) == string::npos || (++colon_pos) >= content_length.size())
    {
      errorCallback_(con, buf, receiveTime, kInvalidContentLength);
      break;
    }
    auto length_num = content_length.substr(colon_pos);
    length_num = Utils::trim(length_num);
    if(!Utils::is_integer(length_num))
    {
      errorCallback_(con, buf, receiveTime, kLengthConvertError);
      break;
    }
    int length = std::stoi(length_num);
    const char* content_begin = Utils::find_content_begin(buf);
    if(content_begin == nullptr)
    {
      LOG_TRACE << "cannot get content yet, wait";
      break;
    }
    if((buf->beginWrite() - content_begin) < length)
    {
      LOG_TRACE << "content is not complete yet! wait";
      break;
    }
    std::string content(content_begin, content_begin + length);
    messageCallback_(con, content, receiveTime);
    buf->retrieveUntil(content_begin + length);
  }
}