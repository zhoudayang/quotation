#ifndef FIT_QUOTATION_STOCK_LIST_HELPER_H
#define FIT_QUOTATION_STOCK_LIST_HELPER_H

#include <boost/noncopyable.hpp>
#include <muduo/net/TcpClient.h>
#include <unordered_map>
#include <muduo/base/CountDownLatch.h>

#include "http_request.h"
#include "codec.h"

namespace fit_stock_sina
{
class stock_list_helper : boost::noncopyable
{
 public:
  enum class Type
  {
    STOCK_ONLY,
    FUND_ONLY,
    BOTH,
  };

  explicit stock_list_helper(Type type = Type::STOCK_ONLY);

  void onConnection(const muduo::net::TcpConnectionPtr& con);

  void onMessage(const muduo::net::TcpConnectionPtr& con, const std::string& message,
                 muduo::Timestamp receiveTime);

  void connect() { http_client_.connect(); }

  void disconnect() { http_client_.disconnect(); }

  int getLatchCount() { return latch_->getCount(); }

  ~stock_list_helper() = default;

  std::vector<std::string> stock_codes() const;

  // 获取更新之后的股票代码
  std::vector<std::string> get_refresh_stock_codes();

 private:
  muduo::net::TcpClient http_client_;
  codec codec_;
  std::unique_ptr<muduo::CountDownLatch> latch_; // use latch to know if data is complete now
  std::vector<std::string> stock_codes_;
  http_request request_;
  Type type_;
};
}
#endif