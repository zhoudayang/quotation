#ifndef FIT_QUOTATION_HTTP_REQUEST_H
#define FIT_QUOTATION_HTTP_REQUEST_H
#include <muduo/net/Buffer.h>
#include <boost/noncopyable.hpp>
#include <string>

namespace fit_stock_sina
{
// a simple wrapper for http_request
class http_request : boost::noncopyable
{
 public:
  http_request();

  // resource url only
  explicit http_request(const std::string& get_url);

  void add_header(const std::string& header, const std::string& content);

  void init_url(const std::string& get_url);

  //fixme: after call buf(), cannot add header again!
  muduo::net::Buffer buf();

 private:
  muduo::net::Buffer buf_;
  bool end_;
};

}
#endif