#ifndef FIT_QUOTATION_SINA_CLIENT_H
#define FIT_QUOTATION_SINA_CLIENT_H

#include <boost/noncopyable.hpp>
#include <muduo/net/TcpClient.h>
#include <muduo/net/TimerId.h>
#include <unordered_map>

#include "http_request.h"
#include "types.h"
#include "codec.h"

namespace fit_stock_sina
{
// 限制输入股票数量为800只, 延时回调，回调发生在IO线程
class sina_client : boost::noncopyable
{
 public:
  typedef std::function<void(std::unordered_map<std::string, struct sina_exposure>, bool)> RunCallback;

  sina_client(const std::vector<std::string>& stock_codes, const RunCallback& runCb, double timeout = 5);

  void onConnection(const muduo::net::TcpConnectionPtr& con);

  void connect() { http_client_.connect(); }

  void disconnect() { http_client_.disconnect(); }

  void onMessageCallback(const muduo::net::TcpConnectionPtr&,
                         const std::string& message, muduo::Timestamp);

  ~sina_client() = default;

  bool complete() const { return complete_; }

 private:

  void onErrorCallback(const muduo::net::TcpConnectionPtr& con,
                       muduo::net::Buffer *,
                       muduo::Timestamp,
                       codec::ErrorCode code);

  void onTimeout();

  std::string get_url() const;

  muduo::net::EventLoop* loop_;
  muduo::net::TcpClient http_client_;
  codec codec_;
  std::vector<std::string> stock_codes_;
  RunCallback runCallback_;
  http_request request_;
  bool complete_;
  std::unique_ptr<muduo::net::TimerId> timerId_;
  bool Callback_runed_;
  muduo::net::TcpConnectionPtr con_;
};

}

#endif
