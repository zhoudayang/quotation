#ifndef FIT_QUOTATION_LOOP_SINGLETON_H
#define FIT_QUOTATION_LOOP_SINGLETON_H

#include <muduo/net/EventLoopThread.h>
#include <boost/noncopyable.hpp>
#include <pthread.h>

namespace fit_stock_sina
{
class loop_singleton : boost::noncopyable
{
 public:
  static loop_singleton* instance();

  muduo::net::EventLoop* loop();

 private:
  loop_singleton();

  ~loop_singleton();

  static void init();

  static void destroy();

  static pthread_once_t ponce_;
  static loop_singleton* value_;

 private:
  muduo::net::EventLoop* loop_;
};
}
#endif
