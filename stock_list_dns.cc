#include "stock_list_dns.h"

#include "quotation_config.h"
#include <muduo/base/Logging.h>

using namespace fit_stock_sina;

pthread_once_t stock_list_dns::ponce_ = PTHREAD_ONCE_INIT;
stock_list_dns* stock_list_dns::value_ = nullptr;

stock_list_dns *stock_list_dns::instance()
{
  pthread_once(&ponce_, &stock_list_dns::init);
  if(value_ == nullptr)
  {
    LOG_FATAL << "create stock_list_dns instance error ";
  }
  return value_;
}

void stock_list_dns::init()
{
  value_ = new stock_list_dns();
  ::atexit(destroy);
}

void stock_list_dns::destroy()
{
  delete value_;
  value_ = nullptr;
}

stock_list_dns::stock_list_dns()
{
  if(!muduo::net::InetAddress::resolve(config::stock_url().c_str(), &addr_))
  {
    LOG_FATAL << "resolve " << config::stock_url() << " error !";
  }
  addr_ = muduo::net::InetAddress(addr_.toIp(), 80);
}

