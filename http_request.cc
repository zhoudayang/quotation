#include "http_request.h"

#include <muduo/base/Logging.h>

using namespace fit_stock_sina;
using std::string;

http_request::http_request()
  : buf_(),
    end_(false)
{

}

http_request::http_request(const std::string &get_url)
   : buf_(),
     end_(false)
{
  init_url(get_url);
}

void http_request::add_header(const std::string &header, const std::string &content)
{
  string data = header + ": " + content + "\r\n";
  buf_.append(data.c_str(), data.size());
}

void http_request::init_url(const std::string &get_url)
{
  string url = "GET " + get_url + " HTTP/1.1\r\n";
  buf_.append(url.c_str(), url.size());
}

muduo::net::Buffer http_request::buf() {
  if(!end_)
  {
    buf_.append("\r\n");
    end_ = true;
  }
  return buf_;
}



