#include "sina_dns.h"

#include "quotation_config.h"
#include <muduo/base/Logging.h>

using namespace fit_stock_sina;

pthread_once_t sina_dns::ponce_ = PTHREAD_ONCE_INIT;
sina_dns* sina_dns::value_ = nullptr;

sina_dns *sina_dns::instance()
{
  pthread_once(&ponce_, &sina_dns::init);
  if(value_ == nullptr)
  {
    LOG_FATAL << "create sina_dns instance error!";
  }
  return value_;
}

void sina_dns::init()
{
  value_ = new sina_dns();
  ::atexit(destroy);
}

void sina_dns::destroy()
{
  delete value_;
  value_ = nullptr;
}

sina_dns::sina_dns()
{
  if(!muduo::net::InetAddress::resolve(config::sina_url().c_str(), &addr_))
  {
    LOG_FATAL << "resolve " << config::sina_url() << " error !";
  }
  addr_ = muduo::net::InetAddress(addr_.toIp(), 80);
}

