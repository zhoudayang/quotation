#include "stock_list_helper.h"
#include "stock_list_dns.h"
#include "loop_singleton.h"
#include "util.h"

#include <muduo/base/Logging.h>
#include <boost/regex.hpp>

using namespace fit_stock_sina;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;

// connect at constructor
stock_list_helper::stock_list_helper(Type type)
  : http_client_(loop_singleton::instance()->loop(), stock_list_dns::instance()->address(), "stock_list_client"),
    codec_(std::bind(&stock_list_helper::onMessage, this, _1, _2, _3)),
    latch_(new muduo::CountDownLatch(1)),
    stock_codes_(),
    request_("/js/lib/astock.js"),
    type_(type)
{
  request_.add_header("HOST", "www.shdjt.com");
  http_client_.setMessageCallback(std::bind(&codec::onMessage, &codec_, _1, _2, _3));
  http_client_.setConnectionCallback(std::bind(&stock_list_helper::onConnection, this, _1));
  connect();
}


void stock_list_helper::onConnection(const muduo::net::TcpConnectionPtr &con)
{
  LOG_INFO << "connection to shdjt is " << (con->connected() ? "Up" :"Down");
  if(con->connected())
  {
    muduo::net::Buffer buf = request_.buf();
    con->send(&buf);
  }
}

void stock_list_helper::onMessage(const muduo::net::TcpConnectionPtr &con,
                                  const std::string &message,
                                  muduo::Timestamp receiveTime)
{
  stock_codes_.clear();
  // 匹配, 但是不包括指定的前缀和后缀
  boost::regex sql_regex("(?<=~)(\\d+)(?=`)");
  boost::sregex_iterator begin(message.begin(), message.end(), sql_regex);
  boost::sregex_iterator end;
  for(auto it = begin; it != end; ++it)
  {
    std::string stock_code = it->str();
    assert(Utils::is_valid_stock_code(stock_code));
    if(type_ == Type::STOCK_ONLY && Utils::is_stock(stock_code))
    {
      stock_codes_.push_back(stock_code);
    }
    else if(type_ == Type::FUND_ONLY && Utils::is_fund(stock_code))
    {
      stock_codes_.push_back(stock_code);
    }
    else if(type_ == Type::BOTH && (Utils::is_stock(stock_code) || Utils::is_fund(stock_code)))
    {
      stock_codes_.push_back(stock_code);
    }
  }
  con->disconnected();
  latch_->countDown();
}


std::vector<std::string> stock_list_helper::stock_codes() const {
  latch_->wait();
  return stock_codes_;
}


std::vector<std::string> stock_list_helper::get_refresh_stock_codes()
{
  latch_.reset(new muduo::CountDownLatch(1));
  disconnect();
  connect();
  return stock_codes();
}
