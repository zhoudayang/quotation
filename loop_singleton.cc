#include "loop_singleton.h"
#include <muduo/base/Logging.h>
#include <muduo/net/EventLoop.h>

#include <muduo/net/EventLoopThread.h>
using namespace fit_stock_sina;

pthread_once_t loop_singleton::ponce_ = PTHREAD_ONCE_INIT;
loop_singleton* loop_singleton::value_ = nullptr;

loop_singleton *loop_singleton::instance() {
  pthread_once(&ponce_, &loop_singleton::init);
  if(value_ == nullptr)
  {
    LOG_FATAL << "create muduo thread loop failed!";
  }
  return value_;
}

muduo::net::EventLoop *loop_singleton::loop() {
  return loop_;
}

loop_singleton::loop_singleton()
{
  static muduo::net::EventLoopThread thread;
  loop_ = thread.startLoop();
}

loop_singleton::~loop_singleton()
{
  loop_->quit();
}

void loop_singleton::init()
{
  value_ = new loop_singleton();
  ::atexit(destroy);
}

void loop_singleton::destroy()
{
  delete value_;
  value_ = nullptr;
}
