#ifndef FTI_QUOTATION_H
#define FIT_QUOTATION_H
#include <string>

namespace fit_stock_sina
{

struct sina_exposure
{
  std::string code;
  std::string name;
  double open;
  double close;
  double now;
  double high;
  double low;
  double buy;
  double sell;
  double turnover;
  double volume;
  double buys[5];
  double buys_volumes[5];
  double sells[5];
  double sells_volumes[5];
  double uplimit = 0;
  double downlimit = 0;
  std::string date;
  std::string time;
};

}
#endif