#ifndef FIT_QUOTATION_STOCK_LIST_DNS_H
#define FIT_QUOTATION_STOCK_LIST_DNS_H
#include <boost/noncopyable.hpp>
#include <muduo/net/InetAddress.h>

// dns getter, blocking way, singleton
namespace fit_stock_sina
{
class stock_list_dns : boost::noncopyable
{
 public:
  static stock_list_dns* instance();

  muduo::net::InetAddress address() const { return addr_; }

 private:
  // constructor
  stock_list_dns();

  // destructor
  ~stock_list_dns() = default;

  static void init();

  static void destroy();

  static pthread_once_t ponce_;
  static stock_list_dns*      value_;

 private:
  muduo::net::InetAddress addr_;
};
}
#endif