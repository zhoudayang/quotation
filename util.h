#ifndef FIT_QUOTATION_UTIL_H
#define FIT_QUOTATION_UTIL_H

#include <muduo/net/Buffer.h>
#include <string>
#include <boost/locale.hpp>

// common share util functions
namespace fit_stock_sina {

namespace Utils {

// find str in Buffer
const char *find(muduo::net::Buffer *buf, const std::string &str);

std::string trim(const std::string &input);

bool is_integer(const std::string &str);

// get content string begin
const char *find_content_begin(muduo::net::Buffer *buf);

bool is_valid_stock_code(const std::string &stock_code);

// true -> sh false -> sz
bool is_sh(const std::string &stock_code);

inline std::string gbk_to_utf8(const std::string &content) {
  return boost::locale::conv::between(content,
                                      "UTF-8",
                                      "GBK");
}

std::vector<std::string> split(const std::string &str, char ch);

bool is_float_number(const std::string &num);

// return true if fund, false for stock
bool is_fund(const std::string &code);

// 包含创业板，A股，中小板
bool is_stock(const std::string &code);

// 包含创业板，A股，中小板, 以及B股
bool is_normal_stock(const std::string &code);

// 是B股吗
bool is_stock_B(const std::string &code);

inline bool is_st(const std::string &name) { return name.find("S") != std::string::npos; }

inline bool is_new(const std::string &name) { return name.substr(0,1) == "N"; }

// 涨停比例 精度
double uplimit(double price, double ratio = 1.1, int accuracy = 2);

// 跌停比例 精度
double downlimit(double price, double ratio = 0.9, int accuracy = 2);

}
}
#endif
