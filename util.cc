#include "util.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

namespace fit_stock_sina {

namespace Utils {

const char *find(muduo::net::Buffer *buf, const std::string &str) {
  const char *crlf = std::search(buf->peek(), const_cast<const char *>(buf->beginWrite()), str.begin(), str.end());
  return crlf == buf->beginWrite() ? nullptr : crlf;
}

std::string trim(const std::string &input) {
  std::string temp(input);
  boost::algorithm::trim(temp);
  return temp;
}

bool is_integer(const std::string &str) {
  const static boost::regex integer_pattern("[0-9]+$");
  return boost::regex_match(str, integer_pattern);
}

const char *find_content_begin(muduo::net::Buffer *buf) {
  if (buf->readableBytes() <= 4) {
    return nullptr;
  }
  static std::string empty_line("\r\n\r\n");
  const char *ptr =
      std::search(buf->peek(), const_cast<const char *>(buf->beginWrite()), empty_line.begin(), empty_line.end());
  if (ptr + 4 < buf->beginWrite()) {
    return ptr + 4;
  }
  return nullptr;
}

bool is_valid_stock_code(const std::string &stock_code) {
  const static boost::regex stock_code_pattern("^\\d{6}$");
  return boost::regex_match(stock_code, stock_code_pattern);
}

bool is_sh(const std::string &code) {
  assert(code.size() == 6);
  assert(is_valid_stock_code(code));
  char front = code.front();
  std::string two = code.substr(0, 2);
  std::string three = code.substr(0, 3);
  std::string four = code.substr(0, 4);

  if (two == "50" || two == "51" || two == "60" || two == "90"
      || three == "110" || three == "113" || three == "132" || three == "204") {
    return true;
  }
  if (two == "00" || two == "13" || two == "18" || two == "15" || two == "16" || two == "18"
      || two == "20" || two == "30" || two == "39" || three == "115" || four == "1318") {
    return false;
  }
  if (front == '5' || front == '6' || front == '9')
    return true;
  return false;
}

std::vector<std::string> split(const std::string &str, char ch) {
  size_t pos1 = 0, pos2 = str.find(ch);
  std::vector<std::string> results;
  while (std::string::npos != pos2) {
    results.push_back(str.substr(pos1, pos2 - pos1));
    pos1 = pos2 + 1;
    pos2 = str.find(ch, pos1);
  }
  if (pos1 != str.size()) {
    results.push_back(str.substr(pos1));
  }
  return results;
}

bool is_float_number(const std::string &num) {
  const static boost::regex float_number_pattern("^(-?\\d+)(\\.\\d+)?$");
  return boost::regex_match(num, float_number_pattern);
}

bool is_fund(const std::string &code) {
  assert(code.size() == 6);
  assert(is_valid_stock_code(code));
  std::string two = code.substr(0, 2);
  std::string three = code.substr(0, 3);
  if (three == "500" || three == "502" || three == "505" || two == "15" || two == "16" || two == "18") {
    return true;
  }
  return false;
}

bool is_stock(const std::string &code) {
  assert(code.size() == 6);
  assert(is_valid_stock_code(code));
  auto three = code.substr(0, 3);
  if (three == "600" || three == "601" || three == "603" || three == "000" || three == "002" || three == "300") {
    return true;
  }
  return false;
}

bool is_normal_stock(const std::string &code) {
  assert(code.size() == 6);
  assert(is_valid_stock_code(code));
  auto three = code.substr(0, 3);
  if (three == "600" || three == "601" || three == "603" || three == "000" ||
      three == "002" || three == "300" || three == "900" || three == "200") {
    return true;
  }
  return false;
}

bool is_stock_B(const std::string &code) {
  assert(code.size() == 6);
  assert(is_valid_stock_code(code));
  auto three = code.substr(0, 3);
  if (three == "900" || three == "200")
    return true;
  return false;
}

double uplimit(double price, double ratio, int accuracy) {
  assert(accuracy >= 1);
  ++ accuracy;
  price *= ratio;
  int multiple = 1 << accuracy;
  while (accuracy--) {
    multiple *= 5;
  }
  price *= multiple;
  int temp_price = static_cast<int>(price);
  int integer = temp_price % 10;
  temp_price -= integer;
  if (integer >= 5)
    temp_price += 10;
  price = static_cast<double>(temp_price) / static_cast<double>(multiple);
  return price;
}

double downlimit(double price, double ratio, int accuracy) {
  assert(accuracy >= 1);
  ++ accuracy;
  price *= ratio;
  int multiple = 1 << accuracy;
  while (accuracy--) {
    multiple *= 5;
  }
  price *= multiple;
  int temp_price = static_cast<int>(price);
  int integer = temp_price % 10;
  temp_price -= integer;
  if (integer >= 5)
    temp_price += 10;
  price = static_cast<double>(temp_price) / static_cast<double>(multiple);
  return price;
}

}
}
