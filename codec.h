#ifndef FIT_QUOTATION_STOCK_LIST_CODEC_H
#define FIT_QUOTATION_STOCK_LIST_CODEC_H

#include <boost/noncopyable.hpp>
#include <muduo/net/Buffer.h>
#include <muduo/net/TcpConnection.h>

namespace fit_stock_sina
{

class codec : boost::noncopyable
{
 public:
  enum ErrorCode
  {
    kNoError = 0,
    kInvalidResponseCode,
    kInvalidContentLength,
    kLengthConvertError,
  };

  typedef std::function<void(const muduo::net::TcpConnectionPtr&, const std::string&, muduo::Timestamp)> MessageCallback;
  typedef std::function<void(const muduo::net::TcpConnectionPtr&, muduo::net::Buffer*, muduo::Timestamp, ErrorCode)> ErrorCallback;

  explicit codec(const MessageCallback& messageCb);

  codec(const MessageCallback& messageCb, const ErrorCallback& errorCb);


  void onMessage(const muduo::net::TcpConnectionPtr& con,
                  muduo::net::Buffer* buf,
                  muduo::Timestamp receiveTime);

  ~codec() = default;

  static void defaultErrorCallback(const muduo::net::TcpConnectionPtr&,
                                   muduo::net::Buffer*,
                                   muduo::Timestamp,
                                   ErrorCode);

 private:

  MessageCallback messageCallback_;
  ErrorCallback errorCallback_;

};
}
#endif