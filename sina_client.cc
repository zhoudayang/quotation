#include "sina_client.h"
#include "sina_dns.h"
#include "util.h"
#include "loop_singleton.h"

#include <muduo/net/EventLoop.h>
#include <muduo/base/Logging.h>

using namespace fit_stock_sina;

using std::placeholders::_1;
using std::placeholders::_2;
using std::placeholders::_3;
using std::placeholders::_4;


sina_client::sina_client(const std::vector<std::string> &stock_codes,
                         const sina_client::RunCallback &runCb,
                         double timeout)
   : loop_(loop_singleton::instance()->loop()),
     http_client_(loop_, sina_dns::instance()->address(), "sina_http"),
     // use self defined error callback function
     codec_(std::bind(&sina_client::onMessageCallback, this, _1, _2, _3), std::bind(&sina_client::onErrorCallback, this, _1, _2, _3, _4)),
     stock_codes_(stock_codes),
     runCallback_(runCb),
     request_(),
     complete_(false),
     Callback_runed_(false)

{
  request_.init_url(get_url());
  http_client_.setMessageCallback(std::bind(&codec::onMessage, &codec_, _1, _2, _3));
  http_client_.setConnectionCallback(std::bind(&sina_client::onConnection, this, _1));
  timerId_.reset(new muduo::net::TimerId(loop_->runAfter(timeout, std::bind(&sina_client::onTimeout, this))));
  connect();
}

std::string sina_client::get_url() const {
  std::string stock_query_str = "/?format=text&list=";
  for (auto &stock_code : stock_codes_) {
    if (Utils::is_sh(stock_code)) {
      stock_query_str += ("sh" + stock_code);
      stock_query_str += ",";
    } else {
      stock_query_str += ("sz" + stock_code);
      stock_query_str += ",";
    }
  }
  return stock_query_str.substr(0, stock_query_str.size() - 1);
}

void sina_client::onConnection(const muduo::net::TcpConnectionPtr &con)
{
  LOG_INFO << "connection to sina is " << (con->connected() ? "Up" :"Down");
  if(con->connected())
  {
    muduo::net::Buffer buf = request_.buf();
    con->send(&buf);
    con_ = con;
  }
  else
  {
    con_.reset();
  }
}

void sina_client::onMessageCallback(const muduo::net::TcpConnectionPtr &con,
                                    const std::string &message,
                                    muduo::Timestamp)
{
  if(timerId_)
  {
    loop_->cancel(*timerId_);
    timerId_.reset();
  }
  auto messages = Utils::split(Utils::gbk_to_utf8(message), '\n');
  std::unordered_map<std::string, struct sina_exposure> exposures;
  exposures.reserve(stock_codes_.size());
  for (auto str : messages)
  {
    std::vector<std::string> elems =  Utils::split(str, ',');
    if(elems.size() == 1)
    {
      LOG_TRACE << elems.front() << " is empty!";
    }
    else if (elems.size() == 33)
    {
      if(elems[0].size() < 9)
      {
        LOG_FATAL << "unknown name type! " << elems.front();
      }
      std::string stock_code = elems.front().substr(2, 6);
      std::string name = elems.front().substr(9);
      double open = Utils::is_float_number(elems[1]) ? std::stod(elems[1]) : 0;
      double close = Utils::is_float_number(elems[2]) ? std::stod(elems[2]) : 0;
      double now = Utils::is_float_number(elems[3]) ? std::stod(elems[3]) : 0;
      double high = Utils::is_float_number(elems[4]) ? std::stod(elems[4]) : 0;
      double low = Utils::is_float_number(elems[5]) ? std::stod(elems[5]) : 0;
      double buy = Utils::is_float_number(elems[6]) ? std::stod(elems[6]) : 0;
      double sell = Utils::is_float_number(elems[7]) ? std::stod(elems[7]) : 0;
      double turnover = Utils::is_float_number(elems[8]) ? std::stod(elems[8]) : 0;
      double volume = Utils::is_float_number(elems[9]) ? std::stod(elems[9]) : 0;

      struct sina_exposure exposure_data;

      for(int i = 10, j = 0; i < 20; ++i, ++j)
      {
        if(j % 2 == 1)
        {
          exposure_data.buys[j/2] = Utils::is_float_number(elems[i]) ? std::stod(elems[i]) : 0;
        }
        else
        {
          exposure_data.buys_volumes[j/2] = Utils::is_float_number(elems[i]) ? std::stod(elems[i]) : 0;
        }
      }

      for(int i = 20, j = 0; i < 30; ++i, ++j)
      {
        if(j % 2 == 1)
        {
          exposure_data.sells[j/2] = Utils::is_float_number(elems[i]) ? std::stod(elems[i]) : 0;
        }
        else
        {
          exposure_data.sells_volumes[j/2] = Utils::is_float_number(elems[i]) ? std::stod(elems[i]) : 0;
        }
      }
      exposure_data.date = elems[30];
      exposure_data.time = elems[31];
      exposure_data.code = stock_code;
      exposure_data.name = name;
      exposure_data.open = open;
      exposure_data.close = close;
      exposure_data.now = now;
      exposure_data.high = high;
      exposure_data.low = low;
      exposure_data.buy = buy;
      exposure_data.sell = sell;
      exposure_data.turnover = turnover;
      exposure_data.volume = volume;

      if(Utils::is_fund(stock_code))
      {
        exposure_data.downlimit = Utils::downlimit(close, 0.9, 3);
        exposure_data.uplimit = Utils::uplimit(close, 1.1, 3);
      }
      else
      {
        if(Utils::is_st(name))
        {
          exposure_data.downlimit = Utils::downlimit(close, 0.95);
          exposure_data.uplimit = Utils::uplimit(close, 1.05);
        }
        else if(Utils::is_new(name))
        {
          exposure_data.downlimit = Utils::downlimit(close, 1.44);
          exposure_data.uplimit = Utils::uplimit(close, 0.56);
        }
        else
        {
          exposure_data.downlimit = Utils::downlimit(close);
          exposure_data.uplimit = Utils::uplimit(close);
        }
      }

      exposures[stock_code] = exposure_data;
    }
    else
    {
      LOG_ERROR << "unknown elems size " << elems.size();
    }
  }
  if(!exposures.empty() && !Callback_runed_)
  {
    Callback_runed_ = true;
    runCallback_(exposures, true);
    LOG_TRACE << "sina_client complete operations!";
  }
  con->forceClose();
  complete_ = true;
  con_.reset();
}

void sina_client::onErrorCallback(const muduo::net::TcpConnectionPtr &con,
                                  muduo::net::Buffer * buf,
                                  muduo::Timestamp time,
                                  codec::ErrorCode code)
{
  if(timerId_)
  {
    loop_->cancel(*timerId_);
    timerId_.reset();
  }
  codec_.defaultErrorCallback(con, buf, time, code);
  if(!Callback_runed_){
    Callback_runed_ = true;
    runCallback_(std::unordered_map<std::string, struct sina_exposure> (), false);
  }
  if(con_)
  {
    con_->forceClose();
    con_.reset();
  }
}

void sina_client::onTimeout()
{
  if(timerId_)
  {
    timerId_.reset();
    LOG_ERROR << "sina client timeout!";
    if(!Callback_runed_) {
      Callback_runed_ = true;
      runCallback_(std::unordered_map<std::string, struct sina_exposure>(), false);
    }
  }
  if(con_)
  {
    con_->forceClose();
    con_.reset();
  }
}
