#ifndef FIT_QUOTATION_CONFIG_H
#define FIT_QUOTATION_CONFIG_H
#include <string>
namespace fit_stock_sina
{
class config
{
 public:
  const static std::string sina_url()
  {
    return "hq.sinajs.cn";
  }

  const static std::string stock_url()
  {
    return "www.shdjt.com";
  }

  ~config() = delete;
  config() = delete;
};

}


#endif
