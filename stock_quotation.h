#ifndef FIT_QUOTATION_STOCK_QUOTATION_H
#define FIT_QUOTATION_STOCK_QUOTATION_H

#include "sina_client.h"
#include <muduo/base/CountDownLatch.h>
#include <unordered_map>

namespace fit_stock_sina
{
// 注：采用此方式查询行情会将流程转换为同步阻塞方式
class stock_quotation : boost::noncopyable, public std::enable_shared_from_this<stock_quotation>
{
 public:

  typedef std::function<void(std::unordered_map<std::string, struct sina_exposure>, bool)> RunCallback;

  explicit stock_quotation(const std::vector<std::string>& stock_codes);

  std::pair<std::unordered_map<std::string, struct sina_exposure>, bool>get_quotations();

  ~stock_quotation();

 private:
  const static int BUCKET_SIZE = 800;

  void runInClient(std::unordered_map<std::string, struct sina_exposure> data, bool complete);

  static void runInClientWeak(const std::weak_ptr<stock_quotation>& wkPtr, std::unordered_map<std::string, struct sina_exposure> data, bool complete);

  void wait();

  static void destructInThread(std::vector<std::shared_ptr<sina_client>> sina_clients);

  void queueInLoop();

  int client_nums_;
  std::vector<std::shared_ptr<sina_client>> sina_clients_;
  std::unique_ptr<muduo::CountDownLatch> latch_;
  std::unordered_map<std::string, struct sina_exposure> quotation_datas_;
  std::vector<std::string> stock_codes_;
  bool complete_;
};
}
#endif
